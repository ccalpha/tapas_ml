#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 15 11:38:56 2017

@author: mayer
"""
#headers
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from scipy import stats
import warnings
from statistics import mean
import operator
import matplotlib
from xgboost import plot_tree
from sklearn import svm
from sklearn.ensemble import RandomForestClassifier
warnings.filterwarnings('ignore')


#files
TRAINING = "sp_mix_training.dat"
TEST = "sp_mix_test.dat"

training_set = pd.read_csv(TRAINING)
test_set = pd.read_csv(TEST)

training_set['label'] = [0 for i in range(training_set.shape[0])]
test_set['label'] = [0 for i in range(test_set.shape[0])]

#features to delete
unrelated_features = ['PM_RUN_CYC', 'PM_RUN_INST_CMPL', 'TP']
expand_name = ['', '.1', '.2', '.3', '.4']

for i in unrelated_features:
    for j in expand_name:
        training_set = training_set.drop(i+j, 1)
        test_set     = test_set.drop(i+j, 1)

#threshold for classification
th1 = 175
th2 = 220

y = training_set.Time
X_train = training_set.drop('Time', 1)
X_test = test_set.drop('Time', 1)
target = test_set.Time

#classify data
train1 = training_set.loc[training_set['Time'] < th1]
train2 = training_set.loc[training_set['Time'] >= th1]
train2 = train2.loc[train2['Time'] < th2]
train3 = training_set.loc[training_set['Time'] >= th2]

#seperated target
y1 = train1.Time
y2 = train2.Time
y3 = train3.Time

#drop time
train1 = train1.drop('Time', 1)
train2 = train2.drop('Time', 1)
train3 = train3.drop('Time', 1)



train1['label'] = [1 for i in range(train1.shape[0])]
train2['label'] = [2 for i in range(train2.shape[0])]
train3['label'] = [3 for i in range(train3.shape[0])]

#finding out maximum value to normalize
all_data = pd.concat((X_train,X_test))

features = list(all_data)
x = []

for i in features:
    x.append(pd.Series.max(all_data[i]))
x.append(1)
all_data = pd.DataFrame.as_matrix(all_data)

train1_matrix = train1.as_matrix()
y1_matrix = y1.as_matrix()
regres_w1 = np.linalg.pinv(train1_matrix).dot(y1_matrix)

train2_matrix = train2.as_matrix()
y2_matrix = y2.as_matrix()
regres_w2 = np.linalg.pinv(train2_matrix).dot(y2_matrix)

train3_matrix = train3.as_matrix()
y3_matrix = y3.as_matrix()
regres_w3 = np.linalg.pinv(train3_matrix).dot(y3_matrix)


'''
a = list(X_test)
dct = {}
for i in range(len(list(a))):
    dct[a[i]] = regres_w3[i] * x[i]
sorted_x = sorted(dct.items(), key=operator.itemgetter(1),reverse=True)
for i in sorted_x:
    print("{0:40} {1}".format(i[0],i[1]))
'''

train_classification = pd.concat((train1, train2, train3))
lab = train_classification.label
train_classification = train_classification.drop('label', 1)

#svm classifier
clf = RandomForestClassifier(n_estimators=10)
clf.fit(train_classification, lab)


X_test = X_test.drop('label', 1)
X_test['label'] = clf.predict(X_test)


linear_preds = []

for index, row in X_test.iterrows():
    if row.label == 1:
        linear_preds.append(row.as_matrix().dot(regres_w1))
    elif row.label == 2:
        linear_preds.append(row.as_matrix().dot(regres_w2))
    else:
        linear_preds.append(row.as_matrix().dot(regres_w3))
avg = 0
for i in range(X_test.shape[0]):
    avg += abs(linear_preds[i] - target[i]) / target[i]
print(avg / X_test.shape[0])

#for i in range(X_test.shape[0]):
    #print("{:4} {:10} {:12}".format(i + 1, target[i], linear_preds[i]))
'''

for i in range(len(features)):
    for j in range(len(features[i])):
        all_data[i][j] /= x[i]

X_train = all_data[0:2400]
X_test  = all_data[2400:]
X_train = pd.DataFrame(X_train)
X_test  = pd.DataFrame(X_test)

all_data = pd.DataFrame(all_data)
'''

#print(linear_preds)
'''
for i in range(600):
    print("{:4} {:10} {:12}".format(i + 1, y2[i], xgb_preds[i]))
'''

#xgb.plot_importance(model_xgb)