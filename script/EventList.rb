def GetEventPath()
    dir = File.dirname(__FILE__)
    return dir+"/../event/events"
end

def EventList(event_file)
    rt = Hash.new()
    File.readlines(event_file).each do |x|
        kv = x.strip.split
        rt[kv[0]] = kv[1]
    end
    return rt
end

def DefaultEventList()
    fname = GetEventPath()
    return EventList(fname)
end

