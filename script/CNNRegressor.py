from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pdb
import numpy as np
import pandas as pd
import itertools
from sklearn import datasets
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing

import os
import sys
import tensorflow as tf
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.training import basic_session_run_hooks
from tensorflow.python.training import session_run_hook
from tensorflow.python.training import training_util
from tensorflow.python.ops import math_ops

from collections import OrderedDict

tf.logging.set_verbosity(tf.logging.INFO)

#implement a network ensemble
def cnn_model_fn(features, labels, mode, params=None):
    hidden_layers = params.get("hidden_layers", [1024])
    dropout_rate = params.get("dropout_rate", 0.0)
    nthread = params.get("nthread", 5)
    nfilter = params.get("nfilter", 256)

    nfeature = int(features["x"].shape.as_list()[1] / nthread)

    input_layer = tf.cast(features["x"], tf.float32)
    #input_layer = tf.reshape(input_layer, [-1]+input_layer.shape.as_list()[1:]+[1])
    input_layer = tf.reshape(input_layer, [-1,nfeature,nthread,1])

    #thread-wise, confederate features of each thread
    conv1 = tf.layers.conv2d(input_layer, filters=nfilter, kernel_size=[nfeature,1], strides=[1,1], padding="valid", activation=tf.nn.relu, name="conv1")
    conv1=tf.reshape(conv1, [-1,nthread,nfilter,1])
    conv2 = tf.layers.conv2d(conv1, filters=nfilter, kernel_size=[1,nfilter], strides=[1,1], padding="valid", activation=tf.nn.relu, name="conv2")
    dense = tf.reshape(conv2, [-1,nthread*nfilter], name="dense_input")
    for i in hidden_layers:
        dense = tf.layers.dense(inputs=dense, units=i,activation=tf.nn.relu)
    if mode == tf.estimator.ModeKeys.TRAIN:
        dense = tf.layers.dropout(inputs=dense, rate=dropout_rate)
    conv_output = tf.identity(dense, name="conv_output")

    #feature-wise, confederate threads of each feature
    #we don't use convolutional layer since the weights of the features may be different
    sliced_features = []
    for i in range(0,nfeature):
        sliced = input_layer[:,i:i+1,:,:]
        sliced = tf.reshape(sliced, [-1,nthread])
        sliced_features += [sliced]
    
    feature_layers = []
    for idx,val in enumerate(sliced_features):
        for j in hidden_layers:
            val = tf.layers.dense(val, units=j, activation=tf.nn.relu, name="feature_{}_{}".format(idx,j))
        val = tf.layers.dropout(val, rate=dropout_rate)
        feature_layers += [val]
    
    output_layer = tf.concat([conv_output]+feature_layers, 1)
    output_layer = tf.layers.dense(output_layer,units=1,activation=None,name="output")
    
    predictions = {"pred" : tf.reshape(output_layer,[-1])}
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    labels = tf.reshape(labels, [-1])
    labels = tf.cast(labels, tf.float32)
    #loss=tf.losses.mean_squared_error(labels, predictions["pred"])
    #loss=tf.losses.absolute_difference(labels, predictions["pred"], weights=100.0*predictions["pred"]/(labels*labels))
    pred = predictions["pred"]
    real = labels
    loss = ((real-pred)**2)/(real**2)*10000.0
    loss=tf.losses.compute_weighted_loss(loss)
    loss=tf.identity(loss,name="loss")

    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    eval_metric_ops = {"mean_absolute_error" : tf.metrics.mean_absolute_error(labels=labels, predictions=predictions["pred"])}
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

TRAINING = "mg_4K_con_training.dat"
TEST = "mg_1K_con_test.dat"

def input_fn(data_set, feature_names, label_name, scalar=None):
    #features={k: data_set[k].values * 1.0 / max(data_set[k].values) for k in feature_names}
    features=OrderedDict()
    if(scalar == None):
        for k in feature_names:
            features[k] = data_set[k].values
    else:
        for k in feature_names:
            features[k] = data_set[k].values * 1.0 / scalar[k]

    labels=data_set[label_name].values
    return features,labels

def main(unused_argv):
    training_set = pd.read_csv(TRAINING)
    test_set = pd.read_csv(TEST)
    col_names = list(training_set)
    #feature_names = col_names[10:-1]
    feature_names = col_names[:-1]
    label_name = col_names[-1]

    scalar = {}
    for i in col_names:
        scalar[i] = max(training_set[i].values)

    regressor = tf.estimator.Estimator(model_fn=cnn_model_fn, params={"hidden_layers" : range(512,0,-32), "dropout_rate" : 0.4},model_dir="/tmp/tf_data/sp_CNN")

    #tensors_to_log = {'value_0' : 'log_tensor_0', 'value_1' : 'log_tensor_1'}
    #tensors_to_log = {'value_0' : 'log_tensor_0'}
    tensors_to_log = {}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=1000)

    train_features,train_label = input_fn(training_set, feature_names, label_name, scalar)
    train_features = np.column_stack(train_features.values())
    test_features,test_label = input_fn(test_set, feature_names, label_name, scalar)
    test_features = np.column_stack(test_features.values())

    for i in range(0,100):
        regressor.train(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":train_features}, y=train_label, batch_size=128, num_epochs=None, shuffle=True), steps=5000, hooks=[logging_hook])
        eval_result = regressor.evaluate(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":test_features}, y=test_label, num_epochs=1, shuffle=False))

        pred_result = regressor.predict(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":test_features}, num_epochs=1, shuffle=False))
        pred = np.array([])
        for i in pred_result:
            pred=np.append(pred, i['pred'])
        pred = pred
        test = test_label

        print(pred)
        print(eval_result)
        print("Error: {:.3f}%".format(np.mean(np.abs(test - pred)/test*100)))
        print("Correlation coefficient: {}".format(np.corrcoef(test,pred)[0,1]))
if __name__ == "__main__":
    tf.app.run()
