#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 14 14:22:00 2017

@author: mayer
"""

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
from scipy.stats import norm
from sklearn.preprocessing import StandardScaler
from scipy import stats
import warnings
from statistics import mean
import operator
warnings.filterwarnings('ignore')

TRAINING = "sp_mix_training.dat"
training_set = pd.read_csv(TRAINING)

#print(training_set.columns)

#print(training_set['Time'].describe())
'''
corrmat = training_set.corr()


k = 10 #number of variables for heatmap
cols = corrmat.nlargest(k, 'Time')['Time'].index
cm = np.corrcoef(training_set[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()
'''
corr = training_set.corr()
corr.sort_values(["Time"], ascending = False, inplace = True)
s = corr.Time
a = sorted(zip(s.index, s), key=lambda x: x[0])

res = {}

for i in range(23):
    t = 0.0
    for j in range(5):
        t += float(a[i * 5 + j][1])
    res[a[5 * i][0]] = t / 5.0
    #print(a[5 * i][0] + ' ' + str(t / 5.0))
    
sorted_x = sorted(res.items(), key=operator.itemgetter(1),reverse=True)    

for i in sorted_x:
    print("{0:40} {1}".format(i[0],i[1]))

