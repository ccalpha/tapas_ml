#!/usr/bin/ruby

require_relative 'EventList.rb'

def ExtractEvents(fname)
    content = File.readlines(fname)
    eventlist = DefaultEventList()

    events = []
    values = []

    #find events
    started = false
    for i in content
        if(i.start_with?("CPU") and started == false)
            started = true
        end
        if(not i.start_with?("CPU") and started == true)
            break
        end
        if(started == true)
            events << i.split[2]
        end
        next
    end

    events.map! do |i|
        i = eventlist.key(i.slice(1,i.length-1))
    end

    counters = Array.new(events.length)
    id = 0
    for i in content
        if(not i.start_with?("CPU"))
            next
        end

        counters[id] = i.split[1].gsub(',','')
        id = id+1
        if(id == events.length)
            values << counters.dup
            id = 0
        end
    end

    return events,values
end

