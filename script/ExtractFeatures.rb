#!/usr/bin/ruby

require_relative 'ExtractEvents'

if ARGV.length != 3
    puts "command tp perf output"
    exit
end

tp = File.readlines(ARGV[0])
event_name,event = ExtractEvents(ARGV[1])
output = `grep \"Time in seconds\" #{ARGV[2]} | awk \'{print $5}\'`.split

#output column name
cnames = ""
#(1..tp[0].split.length).each{|x| cnames += "TP" + x.to_s.rjust(1,'0') + ","}
cnames += "TP,"*tp[0].split.length
cnames += event_name.join(",")
cnames += ",Time"

puts cnames

#output rows
for i in 0...tp.length
    puts tp[i].split.join(',')+','+event[i].join(',')+','+output[i]
end
