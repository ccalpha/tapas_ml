#!/usr/bin/ruby
require_relative 'EventList.rb'
require_relative 'Placement.rb'

events = DefaultEventList()

candidate_events = ["PM_L1_DCACHE_RELOADED_ALL",
                    "PM_DATA_ALL_FROM_L2",
                    "PM_DATA_ALL_FROM_L3",
                    "PM_DATA_ALL_FROM_L21_SHR",
                    "PM_DATA_ALL_FROM_L21_MOD",
                    "PM_DATA_ALL_FROM_L31_SHR",
                    "PM_DATA_ALL_FROM_L31_MOD",
                    "PM_DATA_ALL_FROM_RL2L3_SHR",
                    "PM_DATA_ALL_FROM_RL2L3_MOD",
                    "PM_DATA_ALL_FROM_DL2L3_SHR",
                    "PM_DATA_ALL_FROM_DL2L3_MOD",
                    "PM_DATA_ALL_FROM_LL4",
                    "PM_DATA_ALL_FROM_RL4",
                    "PM_DATA_ALL_FROM_DL4",
                    "PM_DATA_ALL_FROM_LMEM",
                    "PM_DATA_ALL_FROM_RMEM",
                    "PM_DATA_ALL_FROM_DMEM",
                    "PM_DATA_ALL_FROM_ON_CHIP_CACHE",
                    "PM_DATA_ALL_FROM_OFF_CHIP_CACHE",
                    "PM_DATA_ALL_FROM_MEMORY"]

perf_event_list = ""
for i in candidate_events
    perf_event_list.concat "-e r"+events[i]+" "
end

system("perf stat -Aa #{perf_event_list} #{ARGV[0]}")
