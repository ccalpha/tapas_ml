#!/usr/bin/ruby

require 'csv'

if ARGV.length != 2
    puts "usage: command file training_ratio"
    exit
end

content = File.readlines(ARGV[0])
content.uniq!

#hearders in the first line
ntraining = ARGV[1].to_f*(content.length-1)
puts ntraining
training_data_index = (1...content.length).to_a.sample(ntraining)

File.open("training.dat", 'w'){|file|
    file.puts content[0]
    for i in training_data_index
        file.puts content[i]
    end
}

File.open("test.dat", 'w'){|file|
    file.puts content[0]
    for i in 1...content.length
        if not training_data_index.include? i
            file.puts content[i]
        end
    end
}
