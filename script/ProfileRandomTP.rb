#!/usr/bin/ruby
require_relative 'EventList.rb'
require_relative 'Placement.rb'
require_relative 'General.rb'

events = DefaultEventList()

candidate_events = ["PM_L1_DCACHE_RELOADED_ALL",
                    "PM_DATA_ALL_FROM_L2",
                    "PM_DATA_ALL_FROM_L3",
                    "PM_DATA_ALL_FROM_L21_SHR",
                    "PM_DATA_ALL_FROM_L21_MOD",
                    "PM_DATA_ALL_FROM_L31_SHR",
                    "PM_DATA_ALL_FROM_L31_MOD",
                    "PM_DATA_ALL_FROM_RL2L3_SHR",
                    "PM_DATA_ALL_FROM_RL2L3_MOD",
                    "PM_DATA_ALL_FROM_DL2L3_SHR",
                    "PM_DATA_ALL_FROM_DL2L3_MOD",
                    "PM_DATA_ALL_FROM_LL4",
                    "PM_DATA_ALL_FROM_RL4",
                    "PM_DATA_ALL_FROM_DL4",
                    "PM_DATA_ALL_FROM_LMEM",
                    "PM_DATA_ALL_FROM_RMEM",
                    "PM_DATA_ALL_FROM_DMEM",
                    "PM_DATA_ALL_FROM_ON_CHIP_CACHE",
                    "PM_DATA_ALL_FROM_OFF_CHIP_CACHE",
                    "PM_DATA_ALL_FROM_MEMORY",
                    "PM_RUN_INST_CMPL",
                    "PM_RUN_CYC"]

perf_event_list = ""
for i in candidate_events
    perf_event_list.concat "-e r"+events[i]+" "
end

if ARGV.size != 3
    puts "Usage: command prefix exec ntimes"
    exit
end

prefix = ARGV[0]
exec = ARGV[1]
ntimes = ARGV[2].to_i
perf_output_file = GetFileName("#{prefix}_perf")
tp_output_file = GetFileName("#{prefix}_tp")
tp_output = File.open(tp_output_file, 'w')

for i in 0...ntimes
    tp = RandomThreadPlacement(5)
    omp_tp = OMPFormation(tp)
    system({"OMP_PLACES"=>omp_tp, "OMP_SCHEDULE"=>"STATIC"},
            "perf stat -o #{perf_output_file} --append -Aa -C #{tp.join(",")} #{perf_event_list} #{exec}")
    tp_output.write(tp.join(" ")+"\n")
end

tp_output.close
