def GetFileName(base)
    for i in 0...100
        suffix = i.to_s.rjust(3,"0")
        fname = base+"_"+suffix
        if File.exist? fname
            next
        end
        return fname
    end
end
