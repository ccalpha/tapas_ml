#!/usr/bin/Rscript

library(ggplot2)
library(data.table)

#command feature_file
args = commandArgs(trailingOnly=T)
features = read.table(args[1],header=T,sep=',')

features$mem = features$PM_DATA_ALL_FROM_LL4 + features$PM_DATA_ALL_FROM_RL4 + features$PM_DATA_ALL_FROM_DL4 + features$PM_DATA_ALL_FROM_LMEM + features$PM_DATA_ALL_FROM_RMEM + features$PM_DATA_ALL_FROM_DMEM
features$mem_error = features$mem / features$PM_DATA_ALL_FROM_MEMORY
max(features$mem_error)
min(features$mem_error)
mean(features$mem_error)
quit()


#rt = sapply(features, function(x) sd(x)/mean(x))
rt = sapply(features, function(x) quantile(x,0.75)/quantile(x,0.25))
outputs = cbind(colnames(features),unname(rt))
outputs = outputs[order(outputs[,2]),]
x = apply(outputs,1,function(x) paste(x,collapse="\t"))
cat(paste(x,collapse="\n"))

#sort(features$Time)

#col = features$PM_DATA_ALL_FROM_ON_CHIP_CACHE.2
col = features$PM_DATA_ALL_FROM_DMEM.4
max(col)
min(col)
mean(col)

ggplot(features)+
    geom_histogram(aes(x=col),colour="black",fill="white")
