#!/usr/bin/python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import numpy as np
import itertools
import math
from sklearn import datasets
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing

import os
#os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
import tensorflow as tf
import pandas as pd
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.training import basic_session_run_hooks
from tensorflow.python.training import session_run_hook
from tensorflow.python.training import training_util

tf.logging.set_verbosity(tf.logging.ERROR)


TRAINING = "sp_4K_training.dat"
TEST = "sp_1K_test.dat"

nclass = 3

def input_fn_label(data_set, feature_names, label_name):
    features={}
    for i in list(data_set):
        if i in feature_names:
            #    if "TP" in i and i in feature_names:
            features[i] = tf.constant(data_set[i].values/max(data_set[i].values))
            continue

        val=[]
        max_val = max(data_set[i].values)
        min_nonzero = max_val
        for j in data_set[i].values:
            if j == 0:
                continue
            else:
                min_nonzero = min(min_nonzero, j)

        max_val *= 1.01
        diff = (max_val-min_nonzero)/nclass
        for j in data_set[i].values:
            if j == 0:
                val.append(0)
            else:
                val.append(int(math.floor((j-min_nonzero)/diff)+1))

        if i in feature_names:
            features[i] = tf.constant(val)
        elif i == label_name:
            labels=tf.constant(val)
            print(i)
        else:
            continue
    return features,labels

def input_fn_norm(data_set, feature_names, label_name):
    features={}
    for k in feature_names:
        if "TP" in k:
            features[k] = tf.constant(data_set[k].values)
        else:
            #features[k] = tf.constant(data_set[k].values/np.percentile(data_set[k].values, 100)*10000.0)
            features[k] = tf.constant(data_set[k].values)

    label_val=[]
    max_val = max(data_set[label_name].values)*1.1
    min_val = min(data_set[label_name].values)*0.9
    diff = (max_val-min_val)/nclass
    for i in data_set[label_name].values:
        l = 1
        if(i <= 170.0):
            l = 0
        elif(i > 200.0):
            l = 2
        label_val.append(l)
        #label_val.append(int(math.floor((i-min_val)/diff)))
    labels=tf.constant(label_val)
    return features,labels

def input_fn_norm_test(test_set, training_set, feature_names, label_name):
    features={}
    for k in feature_names:
        if "TP" in k:
            features[k] = tf.constant(test_set[k].values)
        else:
            #features[k] = tf.constant(test_set[k].values/np.percentile(training_set[k].values, 100)*10000.0)
            features[k] = tf.constant(test_set[k].values)


    label_val=[]
    max_val = max(training_set[label_name].values)*1.1
    min_val = min(training_set[label_name].values)*0.9
    diff = (max_val-min_val)/nclass
    for i in test_set[label_name].values:
        l = 1
        if(i <= 170.0):
            l = 0
        elif(i > 200.0):
            l = 2
        label_val.append(l)
        #label_val.append(int(math.floor((i-min_val)/diff)))
    labels=tf.constant(label_val)
    return features,labels

def main(unused_argv):
    training_set = pd.read_csv(TRAINING)
    test_set = pd.read_csv(TEST)

    col_names = list(training_set)
    feature_names = col_names[5:-11]
    label_name = col_names[-1]
    feature_column = [tf.contrib.layers.real_valued_column(k) for k in feature_names]

    print(feature_names)
    regressor = tf.contrib.learn.DNNClassifier(feature_columns=feature_column,
            hidden_units=[256,128,64],
            n_classes=nclass,
            optimizer=tf.train.ProximalAdagradOptimizer(learning_rate=0.001),
            #dropout=0.0001,
            #        optimizer=tf.train.GradientDescentOptimizer(0.0001))
            model_dir="/ssddata/tf_data/sp_classifier",
            config=tf.contrib.learn.RunConfig(save_checkpoints_secs=60))

    class ExamplesPerSecondHook(session_run_hook.SessionRunHook):
        def __init__(self,every_n_steps=100, estimator = None, input_fn = None):
            if (every_n_steps is None):
                raise ValueError('exactly one of every_n_steps and every_n_secs should be provided.')
            self._timer = basic_session_run_hooks.SecondOrStepTimer(every_steps=every_n_steps)
            self._step_train_time = 0
            self._total_steps = 0
            self.input_fn = input_fn
            self.estimator = estimator

        def begin(self):
            self._global_step_tensor = training_util.get_global_step()
            self._loss_tensor = tf.losses.get_total_loss()
            if self._global_step_tensor is None:
                raise RuntimeError('Global step should be created to use StepCounterHook.')

        def before_run(self, run_context):  # pylint: disable=unused-argument
            return basic_session_run_hooks.SessionRunArgs({"global_step":self._global_step_tensor, "loss":self._loss_tensor})

        def after_run(self, run_context, run_values):
            _ = run_context

            global_step = run_values.results['global_step']
            global_loss = run_values.results['loss']
            if self._timer.should_trigger_for_step(global_step):
                elapsed_time, elapsed_steps = self._timer.update_last_triggered_step(global_step)
                if elapsed_time is not None:
                    steps_per_sec = elapsed_steps / elapsed_time
                    self._step_train_time += elapsed_time
                    self._total_steps += elapsed_steps
                    if((self.input_fn is not None) and (self.estimator is not None)):
                        ev = self.estimator.evaluate(input_fn=self.input_fn,steps=1)
                        print('Step {0} : global loss {1}, test loss {2}, accuracy {3}'.format(global_step, global_loss, ev['loss'], ev['accuracy']))
                    else:
                        print('Step {0} : global loss {1}'.format(global_step, global_loss))
                    logging.info('Loss at step %g : %g', self._total_steps, global_loss)

    hooks = [ExamplesPerSecondHook(every_n_steps=1000, estimator=regressor, input_fn=lambda: input_fn_norm_test(test_set, training_set, feature_names, label_name))]
    regressor.fit(input_fn=lambda: input_fn_norm(training_set,feature_names,label_name), steps=1000000, monitors=hooks)
    accuracy_score = regressor.evaluate(input_fn=lambda: input_fn_norm_test(test_set, training_set, feature_names, label_name),steps=1)

    print(accuracy_score)
    #print("Accurancy: {0:f}\n".format(accuracy_score))

    pred = regressor.predict(input_fn=lambda: input_fn_norm_test(test_set,training_set,feature_names,label_name))

    #print test labels
    labels = input_fn_norm_test(test_set, training_set, feature_names, label_name)[1]
    test_labels_print = tf.Print(labels, [labels])
    v = tf.Session().run(test_labels_print)
    print("Test labels: {}".format(str(list(v))))

    print(accuracy_score)
    print("Predictions: {}".format(str(list(pred))))

if __name__ == "__main__":
    tf.app.run()
