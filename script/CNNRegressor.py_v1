from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pdb
import numpy as np
import pandas as pd
import itertools
from sklearn import datasets
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing

import os
import sys
import tensorflow as tf
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.training import basic_session_run_hooks
from tensorflow.python.training import session_run_hook
from tensorflow.python.training import training_util
from collections import OrderedDict

tf.logging.set_verbosity(tf.logging.INFO)

#implement a network ensemble
def cnn_model_fn(features, labels, mode, params=None):
    hidden_layers = params.get("hidden_layers", [1024])
    dropout_rate = params.get("dropout_rate", 0.0)
    nthread = params.get("nthread", 5)
    nfilter = params.get("nfilter", 128)

    nfeature = int(features["x"].shape.as_list()[1] / nthread)

    input_layer = tf.cast(features["x"], tf.float32)
    #input_layer = tf.reshape(input_layer, [-1]+input_layer.shape.as_list()[1:]+[1])
    input_layer = tf.reshape(input_layer, [-1,nfeature,nthread,1])

    conv1 = tf.layers.conv2d(input_layer, filters=nfilter, kernel_size=[nfeature,1], strides=[1,1], padding="valid", activation=tf.nn.relu, name="conv1")
    conv1=tf.reshape(conv1, [-1,nthread,nfilter,1])
    conv2 = tf.layers.conv2d(conv1, filters=nfilter, kernel_size=[1,nfilter], strides=[1,1], padding="valid", activation=tf.nn.relu, name="conv2")
    dense = tf.reshape(conv2, [-1,nthread*nfilter], name="dense_input") 
    for i in hidden_layers:
        dense = tf.layers.dense(inputs=dense, units=i,activation=tf.nn.relu)
    if mode == tf.estimator.ModeKeys.TRAIN:
        dense = tf.layers.dropout(inputs=dense, rate=dropout_rate)
    conv_output = tf.identity(dense, name="conv_output")

    output_layer = tf.layers.dense(conv_output, units=1, activation=none)
    predictions = {"pred" : tf.reshape(output_layer,[-1])}
    if mode == tf.estimator.ModeKeys.PREDICT:
        return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

    labels = tf.reshape(labels, [-1])
    labels = tf.cast(labels, tf.float32)
    loss=tf.losses.mean_squared_error(labels, predictions["pred"])
    loss=tf.identity(loss,name="log_tensor_0")
    if mode == tf.estimator.ModeKeys.TRAIN:
        optimizer = tf.train.GradientDescentOptimizer(learning_rate=0.00005)
        train_op = optimizer.minimize(loss=loss, global_step=tf.train.get_global_step())
        return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)
    eval_metric_ops = {"mean_absolute_error" : tf.metrics.mean_absolute_error(labels=labels, predictions=predictions["pred"])}
    return tf.estimator.EstimatorSpec(mode=mode, loss=loss, eval_metric_ops=eval_metric_ops)

TRAINING = "sp_4K_training.dat"
TEST = "sp_1K_test.dat"

def input_fn(data_set, feature_names, label_name):
    #features={k: data_set[k].values * 1.0 / max(data_set[k].values) for k in feature_names}
    features=OrderedDict()
    for k in feature_names:
        features[k] = data_set[k].values * 1.0 / max(data_set[k].values)
    labels=data_set[label_name].values
    return features,labels

def main(unused_argv):
    training_set = pd.read_csv(TRAINING)
    test_set = pd.read_csv(TEST)
    col_names = list(training_set)
    #feature_names = col_names[10:-1]
    feature_names = col_names[10:-11]
    label_name = col_names[-1]

    regressor = tf.estimator.Estimator(model_fn=cnn_model_fn, params={"hidden_layers" : range(1024,0,-64)})

    #tensors_to_log = {'value_0' : 'log_tensor_0', 'value_1' : 'log_tensor_1'}
    tensors_to_log = {'value_0' : 'log_tensor_0'}
    logging_hook = tf.train.LoggingTensorHook(tensors=tensors_to_log, every_n_iter=100)

    train_features,train_label = input_fn(training_set, feature_names, label_name)
    train_features = np.column_stack(train_features.values())
    regressor.train(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":train_features}, y=train_label, batch_size=128, num_epochs=None, shuffle=True), steps=1000, hooks=[logging_hook])

    test_features,test_label = input_fn(test_set, feature_names, label_name)
    test_features = np.column_stack(test_features.values())
    eval_result = regressor.evaluate(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":test_features}, y=test_label, num_epochs=1, shuffle=False))

    pred_result = regressor.predict(input_fn=tf.estimator.inputs.numpy_input_fn(x={"x":test_features}, num_epochs=1, shuffle=False))
    print(list(pred_result))
    #pred_result = np.array(list(pred_result))
    #print(pred_result)

if __name__ == "__main__":
    tf.app.run()
