`EventList.rb` : get event list

`ExtractEvents.rb` : extract event vector from perf output

`ExtractFeatures.rb` : extract feature vector from three files, thread placement, perf output and program output.

`General.rb` : auxiliary functions

`Placement.rb` : generate random thread placement and transfer a placement into OpenMP formation

`Profile.rb` : Profile program and collecting data. Usage: `Profile.rb program`

`ProfileRandomTP.rb` : profile program with random thread placement, useage is the same as `Profile.rb`

`Analysis.r` : analysis feature values, like standard derivation

`DNNRegressor` : Deep neural network regressor

## TODO
* `Label.rb` : labelize feature values

