$P8CORE=Array((0...160).step(8))
def RandomThreadPlacement(nthread)
    rand = $P8CORE.shuffle
    rand = rand.slice(0,nthread)
    return rand
end

def TP2OMP(place)
    rt = "{"+place.join("},{")+"}"
    return rt
end

def OMP2TP(place)
    place.delete!("{}")
    rt=place.split(',')
    return rt.join(" ")
end

