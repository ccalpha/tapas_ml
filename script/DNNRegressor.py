#!/usr/bin/python
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function


import numpy as np
import itertools
import argparse
from sklearn import datasets
from sklearn import metrics
from sklearn import model_selection
from sklearn import preprocessing

import os
import sys
#os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
import tensorflow as tf
from tensorflow.python.platform import tf_logging as logging
from tensorflow.python.training import basic_session_run_hooks
from tensorflow.python.training import session_run_hook
from tensorflow.python.training import training_util
import pandas as pd

tf.logging.set_verbosity(tf.logging.ERROR)
#tf.logging.set_verbosity(tf.logging.INFO)

#TRAINING = "sp_training"
#TEST = "sp_test"

#TRAINING = "sp_mix_training.dat"
#TEST = "sp_mix_test.dat"
TRAINING = "sp_4K_training.dat"
TEST = "sp_1K_test.dat"

def input_fn(data_set, feature_names, label_name):
    features={k: tf.constant(data_set[k].values) for k in feature_names}
    labels=tf.constant(data_set[label_name].values)
    return features,labels

def input_fn_norm(data_set, feature_names, label_name):
    features={}
    for k in feature_names:
        if "TP" in k:
            features[k] = tf.constant(data_set[k].values)
        else:
            features[k] = tf.constant(data_set[k].values/np.percentile(data_set[k].values, 100))
    labels=tf.constant(np.power(300.0-data_set[label_name].values, 2))
    return features,labels

def input_fn_norm_test(training_set, test_set, feature_names, label_name):
    features={}
    for k in feature_names:
        if "TP" in k:
            features[k] = tf.constant(test_set[k].values)
        else:
            features[k] = tf.constant(test_set[k].values/np.percentile(training_set[k].values, 100))
    labels=tf.constant(np.power(300.0-test_set[label_name].values, 2))
    return features,labels


def aggregate_events(features):
    col_names = list(features)

    #aggregate SHR and MOD
    for i in range(20,25): #L21
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+5]].values
    for i in range(30,35): #L31
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+5]].values
    for i in range(40,45): #RL2L3
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+5]].values
    for i in range(50,55): #DL2L3
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+5]].values

    #aggreate L4 and MEM
    for i in range(60,75):
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+15]].values

    #aggregate L2L3
    for i in range(10,15):
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+5]].values
    for i in range(20,25):
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+10]].values

    #onchip access
    for i in range(10,15):
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+10]].values
    #offchip acess
    for i in range(40,45):
        features[col_names[i]] = features[col_names[i]].values + features[col_names[i+10]].values

def main(unused_argv):
    steps = 1000000
    if(len(unused_argv) >= 2):
        steps = int(unused_argv[1])

    training_set = pd.read_csv(TRAINING)
    test_set = pd.read_csv(TEST)

    aggregate_events(training_set)
    aggregate_events(test_set)

    col_names = list(training_set)
#    feature_names = col_names[0:-1]
#    feature_names = col_names[5:105]
#    feature_names = col_names[10:60]+col_names[90:100]
#    feature_names = col_names[10:25]+col_names[30:35]+col_names[40:45]+col_names[50:55]+col_names[60:75]#+col_names[90:100]
    #feature_names = col_names[10:15]+col_names[20:25]+col_names[40:45]+col_names[50:55]+col_names[60:75]
    feature_names = col_names[10:15]+col_names[40:45]+col_names[60:75]
    label_name = col_names[-1]
#    print(feature_names)
    feature_column = [tf.contrib.layers.real_valued_column(k) for k in feature_names]


    regressor = tf.contrib.learn.DNNRegressor(feature_columns=feature_column,
            #hidden_units=[64,60,56,52,48,44,40,36,32,28,24,20,16,12,8,4],
            hidden_units=range(256,0,-32),
            optimizer=tf.train.ProximalAdagradOptimizer(learning_rate=0.005, l2_regularization_strength=0.000),
            dropout=0.002,
            #activation_fn=tf.sigmoid,
            model_dir="/tmp/tf_data/sp_regressor_V10_5K",
            config=tf.contrib.learn.RunConfig(save_checkpoints_secs=10))


    class ExamplesPerSecondHook(session_run_hook.SessionRunHook):
        def __init__(self,every_n_steps=100, estimator = None, input_fn = None):
            if (every_n_steps is None):
                raise ValueError('exactly one of every_n_steps and every_n_secs should be provided.')
            self._timer = basic_session_run_hooks.SecondOrStepTimer(every_steps=every_n_steps)
            self._step_train_time = 0
            self._total_steps = 0
            self.input_fn = input_fn
            self.estimator = estimator

        def begin(self):
            self._global_step_tensor = training_util.get_global_step()
            self._loss_tensor = tf.losses.get_total_loss()
            if self._global_step_tensor is None:
                raise RuntimeError('Global step should be created to use StepCounterHook.')

        def before_run(self, run_context):  # pylint: disable=unused-argument
            return basic_session_run_hooks.SessionRunArgs({"global_step":self._global_step_tensor, "loss":self._loss_tensor})

        def after_run(self, run_context, run_values):
            _ = run_context

            global_step = run_values.results['global_step']
            global_loss = run_values.results['loss']
            if self._timer.should_trigger_for_step(global_step):
                elapsed_time, elapsed_steps = self._timer.update_last_triggered_step(global_step)
                if elapsed_time is not None:
                    steps_per_sec = elapsed_steps / elapsed_time
                    self._step_train_time += elapsed_time
                    self._total_steps += elapsed_steps
                    if((self.input_fn is not None) and (self.estimator is not None)):
                        ev = self.estimator.evaluate(input_fn=self.input_fn,steps=1)
                        print('Step {0} : global loss {1} test loss {2}'.format(global_step, global_loss, ev['loss']))

                        #pred = self.estimator.evaluate(input_fn=self.input_fn)
                        #tf.contrib.keras.losses.mean_absolute_error(input_fn()[1], pred)
                    else:
                        print('Step {0} : global loss {1}'.format(global_step, global_loss))
                    logging.info('Loss at step %g : %g', self._total_steps, global_loss)

    hooks = [ExamplesPerSecondHook(every_n_steps=1000, estimator=regressor, input_fn=lambda: input_fn_norm_test(training_set, test_set,feature_names,label_name))]
    regressor.fit(input_fn=lambda: input_fn_norm(training_set,feature_names,label_name), steps=steps, monitors=hooks)
    ev = regressor.evaluate(input_fn=lambda: input_fn_norm_test(training_set, test_set,feature_names,label_name),steps=1)
    #ev = regressor.evaluate(input_fn=lambda: input_fn_norm(training_set,feature_names,label_name),steps=1)

    loss_score = ev["loss"]
    print("Loss: {0:f}".format(loss_score))

    pred = regressor.predict(input_fn=lambda: input_fn_norm_test(training_set, test_set,feature_names,label_name))
    #predictions = list(itertools.islice(pred, 8))
    #pred_values = np.percentile(training_set[label_name].values, 100) - np.power(list(pred),2)
    pred_values = 300.0-np.sqrt(np.asarray(list(pred)))
    print(test_set[label_name].values)
    print("Loss: {0:f}".format(loss_score))
    print("Predictions: {}".format(str(pred_values)))
    print("Error: {:.3f}%".format(np.mean(np.abs(test_set[label_name].values - pred_values)/test_set[label_name]*100)))

if __name__ == "__main__":
    tf.app.run()
