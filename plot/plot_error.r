#!/usr/local/bin/Rscript

require('ggplot2')
library('reshape')

#command data_file
args = commandArgs(trailingOnly=TRUE)
dat = read.table(args[1], header=T)
eruption.lm = lm(Real ~ Pred, data=dat)
summary(eruption.lm)

dat = dat[order(dat$Real),]
dat$order = seq(1,nrow(dat))

dat_melt = melt(dat,id=c("ID", "order"))
head(dat_melt)

time_plot = ggplot(dat_melt)+
  geom_point(aes(x=order, y=value, color=variable))+
  labs(x="Execution of Program SP", y="Time (s)")+
  theme(legend.title=element_blank())+
  scale_y_continuous(limits=c(min(dat$Real)*0.9, max(dat$Real)*1.1))+
  coord_fixed(ratio=2.5)
  

filename=tools::file_path_sans_ext(args[1])
ggsave(paste0(filename,".pdf"), time_plot, device="pdf")
