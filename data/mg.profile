Bare run, no thread binding or data binding. Test the execution time.

 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     1

 Initialization time:          33.999 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857053E-07
 Error is    6.0782697075039E-12


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    59.39
 Total threads   =                        1
 Avail threads   =                        1
 Mop/s total     =                  2621.62
 Mop/s/thread    =                  2621.62
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     2

 Initialization time:          14.925 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857306E-07
 Error is    1.6434575583489E-12


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    30.85
 Total threads   =                        2
 Avail threads   =                        2
 Mop/s total     =                  5046.83
 Mop/s/thread    =                  2523.41
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     3

 Initialization time:          10.133 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857344E-07
 Error is    9.8110223171699E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    21.58
 Total threads   =                        3
 Avail threads   =                        3
 Mop/s total     =                  7215.82
 Mop/s/thread    =                  2405.27
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     4

 Initialization time:           7.763 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857366E-07
 Error is    6.0446881069099E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    19.68
 Total threads   =                        4
 Avail threads   =                        4
 Mop/s total     =                  7910.62
 Mop/s/thread    =                  1977.66
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     5

 Initialization time:           6.326 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857373E-07
 Error is    4.6680280162631E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    14.72
 Total threads   =                        5
 Avail threads   =                        5
 Mop/s total     =                 10574.61
 Mop/s/thread    =                  2114.92
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     6

 Initialization time:           5.372 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857387E-07
 Error is    2.3061839523907E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.43
 Total threads   =                        6
 Avail threads   =                        6
 Mop/s total     =                 11589.22
 Mop/s/thread    =                  1931.54
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     7

 Initialization time:           4.868 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857388E-07
 Error is    2.0687008100689E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    14.59
 Total threads   =                        7
 Avail threads   =                        7
 Mop/s total     =                 10668.45
 Mop/s/thread    =                  1524.06
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     8

 Initialization time:           4.451 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857394E-07
 Error is    1.1169129037323E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    14.91
 Total threads   =                        8
 Avail threads   =                        8
 Mop/s total     =                 10439.01
 Mop/s/thread    =                  1304.88
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:     9

 Initialization time:           4.170 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857395E-07
 Error is    9.0540448010191E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    14.72
 Total threads   =                        9
 Avail threads   =                        9
 Mop/s total     =                 10578.62
 Mop/s/thread    =                  1175.40
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    10

 Initialization time:           3.837 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857394E-07
 Error is    1.1336109371768E-13


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.29
 Total threads   =                       10
 Avail threads   =                       10
 Mop/s total     =                 11719.11
 Mop/s/thread    =                  1171.91
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    11

 Initialization time:           3.751 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857397E-07
 Error is    5.6031178891553E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    14.09
 Total threads   =                       11
 Avail threads   =                       11
 Mop/s total     =                 11052.71
 Mop/s/thread    =                  1004.79
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    12

 Initialization time:           3.689 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857399E-07
 Error is    2.1336376067975E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.58
 Total threads   =                       12
 Avail threads   =                       12
 Mop/s total     =                 11468.40
 Mop/s/thread    =                   955.70
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    13

 Initialization time:           3.461 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857399E-07
 Error is    1.9666572723525E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    12.87
 Total threads   =                       13
 Avail threads   =                       13
 Mop/s total     =                 12100.81
 Mop/s/thread    =                   930.83
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    14

 Initialization time:           3.380 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857399E-07
 Error is    1.8182303084014E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    12.48
 Total threads   =                       14
 Avail threads   =                       14
 Mop/s total     =                 12470.93
 Mop/s/thread    =                   890.78
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    15

 Initialization time:           3.333 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857399E-07
 Error is    1.6698033444503E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    12.71
 Total threads   =                       15
 Avail threads   =                       15
 Mop/s total     =                 12249.78
 Mop/s/thread    =                   816.65
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    16

 Initialization time:           3.477 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857401E-07
 Error is    1.5399297509930E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    12.66
 Total threads   =                       16
 Avail threads   =                       16
 Mop/s total     =                 12294.43
 Mop/s/thread    =                   768.40
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    17

 Initialization time:           3.194 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857402E-07
 Error is    2.9128791675410E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    12.19
 Total threads   =                       17
 Avail threads   =                       17
 Mop/s total     =                 12775.15
 Mop/s/thread    =                   751.48
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    18

 Initialization time:           3.117 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857401E-07
 Error is    2.5974718691448E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.00
 Total threads   =                       18
 Avail threads   =                       18
 Mop/s total     =                 11973.13
 Mop/s/thread    =                   665.17
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    19

 Initialization time:           3.306 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857401E-07
 Error is    1.6141432329686E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.46
 Total threads   =                       19
 Avail threads   =                       19
 Mop/s total     =                 11569.46
 Mop/s/thread    =                   608.92
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------



 NAS Parallel Benchmarks (NPB3.3-OMP-C) - MG Benchmark

 No input file. Using compiled defaults 
 Size:  512x 512x 512  (class C)
 Iterations:                     20
 Number of available threads:    20

 Initialization time:           3.462 seconds

ROI begins
  iter   1
  iter   5
  iter  10
  iter  15
  iter  20
ROI ends

 Benchmark completed
 VERIFICATION SUCCESSFUL
 L2 Norm is  5.7067322857401E-07
 Error is    2.1707443477853E-14


 MG Benchmark Completed.
 Class           =                        C
 Size            =            512x 512x 512
 Iterations      =                       20
 Time in seconds =                    13.85
 Total threads   =                       20
 Avail threads   =                       20
 Mop/s total     =                 11244.88
 Mop/s/thread    =                   562.24
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = randdp

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------

