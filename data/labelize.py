import numpy as np
with open('mg_0.txt') as f:
    lines = f.readlines()

data = []
for i in lines:
	line = i.replace(' \n', '').split(',')
	line[-1] = round(float(line[-1]))
	line = map(int, line)
	data.append(line)

# print  len(data[0]), len(data)

data = np.array(data)
data_trans = data.transpose()

def labelize(list, bin):
	if np.min(list) != 0:
		# nz = min(x for x in list if x != 0)
		res = np.round((list - min(list) ) / ((np.max(list) - np.min(list)) / float(bin - 1)))
		res = res.astype(int)
		return res
	else:
		nz = min(x for x in list if x != 0)
		# print "nz",nz
		# res = np.round((list - nz) / (float(np.max(list) - nz) / (bin -2)))
		res = ((list - nz) / (float(np.max(list) - nz) / (bin -2)))
		res = (res.clip(min = -1) + 1).astype(int)
		# print res
		return res


labelized_data = []
# labelized_data = np.array(labelized_data)
for i in data_trans:
	labelized_data.append(labelize(i, 101))

labelized_data = np.array(labelized_data)

res = labelized_data.transpose()


np.savetxt('mg_0_label.txt', res, delimiter=',', fmt='%i')


