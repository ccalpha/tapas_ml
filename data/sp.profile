Bare run, no thread binding or data binding. Test the execution time.

cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     1

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818293E+02 5.8816915818290E+02 5.9339832248876E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.0876757950007E-14
           3 3.2938291918508E+02 3.2938291918510E+02 5.2980675602250E-14
           4 3.0819249718905E+02 3.0819249718910E+02 1.4976631986812E-13
           5 4.5972237991759E+02 4.5972237991760E+02 2.4113583985495E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 8.7600140190621E-15
           2 2.5908889223144E-02 2.5908889223150E-02 2.3541293835494E-13
           3 5.1328864163196E-02 5.1328864163200E-02 6.9485104039481E-14
           4 4.8060734194540E-02 4.8060734194540E-02 5.1975939345729E-15
           5 5.4833774913005E-01 5.4833774913010E-01 8.5442615821194E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   416.04
 Total threads   =                        1
 Avail threads   =                        1
 Mop/s total     =                  3485.53
 Mop/s/thread    =                  3485.53
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	6m59.996s
user	6m56.388s
sys	0m3.601s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     2

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 6.7457985194977E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.3308519754398E-14
           3 3.2938291918509E+02 3.2938291918510E+02 4.3489023621391E-14
           4 3.0819249718905E+02 3.0819249718910E+02 1.4958187858749E-13
           5 4.5972237991759E+02 4.5972237991760E+02 2.5014698094178E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.7306369159610E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.5680804938205E-13
           3 5.1328864163197E-02 5.1328864163200E-02 5.8129561745091E-14
           4 4.8060734194539E-02 4.8060734194540E-02 2.5410459235690E-14
           5 5.4833774913006E-01 5.4833774913010E-01 6.5600491767931E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   241.44
 Total threads   =                        2
 Avail threads   =                        2
 Mop/s total     =                  6006.17
 Mop/s/thread    =                  3003.09
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	4m3.585s
user	7m51.200s
sys	0m15.922s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     3

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.2870087159044E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.3308519754398E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.8656909885681E-14
           4 3.0819249718905E+02 3.0819249718910E+02 1.4736858321998E-13
           5 4.5972237991759E+02 4.5972237991760E+02 2.3933361163758E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.9229299066234E-14
           2 2.5908889223145E-02 2.5908889223150E-02 1.8827678687545E-13
           3 5.1328864163198E-02 5.1328864163200E-02 4.1501803385448E-14
           4 4.8060734194540E-02 4.8060734194540E-02 5.7751043717476E-16
           5 5.4833774913007E-01 5.4833774913010E-01 6.2765902617465E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   171.19
 Total threads   =                        3
 Avail threads   =                        3
 Mop/s total     =                  8470.47
 Mop/s/thread    =                  2823.49
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	2m52.694s
user	8m26.077s
sys	0m11.915s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     4

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.3256665870763E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.5624483377628E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.7794032432876E-14
           4 3.0819249718905E+02 3.0819249718910E+02 1.4810634834248E-13
           5 4.5972237991759E+02 4.5972237991760E+02 2.4293806807231E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 6.4097663554113E-15
           2 2.5908889223146E-02 2.5908889223150E-02 1.5908451124327E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.4877737047054E-14
           4 4.8060734194539E-02 4.8060734194540E-02 1.7325313115243E-14
           5 5.4833774913006E-01 5.4833774913010E-01 6.8232610264793E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   160.71
 Total threads   =                        4
 Avail threads   =                        4
 Mop/s total     =                  9023.01
 Mop/s/thread    =                  2255.75
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	2m41.966s
user	9m55.279s
sys	0m52.446s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     5

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.4223112650060E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.6435070645759E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.6240853017826E-14
           4 3.0819249718905E+02 3.0819249718910E+02 1.4773746578123E-13
           5 4.5972237991759E+02 4.5972237991760E+02 3.1863165320173E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001831E-01 2.5981205001830E-01 2.7348336449755E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6350352544447E-13
           3 5.1328864163198E-02 5.1328864163200E-02 4.4340688959046E-14
           4 4.8060734194540E-02 4.8060734194540E-02 6.6413700275098E-15
           5 5.4833774913006E-01 5.4833774913010E-01 6.8435080918397E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   173.71
 Total threads   =                        5
 Avail threads   =                        5
 Mop/s total     =                  8347.78
 Mop/s/thread    =                  1669.56
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	2m54.863s
user	11m22.794s
sys	3m11.340s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     6

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.4996270073498E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.6319272464597E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.6758579489509E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4552417041372E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.2584056607120E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.1964897196768E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.5801323507328E-13
           3 5.1328864163197E-02 5.1328864163200E-02 4.9883275078927E-14
           4 4.8060734194539E-02 4.8060734194540E-02 1.1405831134202E-14
           5 5.4833774913007E-01 5.4833774913010E-01 6.3170843924675E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   120.30
 Total threads   =                        6
 Avail threads   =                        6
 Mop/s total     =                 12053.58
 Mop/s/thread    =                  2008.93
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	2m1.244s
user	10m40.793s
sys	1m26.439s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     7

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.3256665870763E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8172043363181E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.7448881451754E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4460196401059E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3845616359277E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.5597098131501E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6430698257197E-13
           3 5.1328864163199E-02 5.1328864163200E-02 2.7712930599404E-14
           4 4.8060734194539E-02 4.8060734194540E-02 1.0539565478439E-14
           5 5.4833774913006E-01 5.4833774913010E-01 8.0380849481076E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   128.21
 Total threads   =                        7
 Avail threads   =                        7
 Mop/s total     =                 11310.42
 Mop/s/thread    =                  1615.77
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	2m9.300s
user	12m51.043s
sys	2m13.742s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     8

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.3836533938341E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8982630631311E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.7276305961193E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4515528785247E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4566507646224E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.7733686916638E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6511043969946E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.8122177702594E-14
           4 4.8060734194539E-02 4.8060734194540E-02 2.0790375738291E-14
           5 5.4833774913006E-01 5.4833774913010E-01 6.6612845035955E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    83.98
 Total threads   =                        8
 Avail threads   =                        8
 Mop/s total     =                 17267.91
 Mop/s/thread    =                  2158.49
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m24.712s
user	10m36.344s
sys	0m41.037s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:     9

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.1903640379746E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8287841544342E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.6758579489509E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4441752272997E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4746730467961E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.6024415888528E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6685126347569E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.7446252566024E-14
           4 4.8060734194540E-02 4.8060734194540E-02 1.0250810259852E-14
           5 5.4833774913006E-01 5.4833774913010E-01 7.0864728761654E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   105.82
 Total threads   =                        9
 Avail threads   =                        9
 Mop/s total     =                 13703.60
 Mop/s/thread    =                  1522.62
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m46.816s
user	13m12.800s
sys	2m47.903s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    10

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.1323772312167E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.9214226993634E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.5723126546143E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4386419888809E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3665393537540E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.7947345795152E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6417307305072E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.4877737047054E-14
           4 4.8060734194540E-02 4.8060734194540E-02 5.4863491531603E-15
           5 5.4833774913006E-01 5.4833774913010E-01 6.9447434186421E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    99.18
 Total threads   =                       10
 Avail threads   =                       10
 Mop/s total     =                 14621.60
 Mop/s/thread    =                  1462.16
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m40.148s
user	13m44.467s
sys	2m56.234s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    11

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0937193600448E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8519437906665E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.4515098112215E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4312643376559E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3665393537540E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.1323920561227E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.6966336342191E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.6905512456767E-14
           4 4.8060734194540E-02 4.8060734194540E-02 9.3845446040899E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.0054846147235E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   100.40
 Total threads   =                       11
 Avail threads   =                       11
 Mop/s total     =                 14443.78
 Mop/s/thread    =                  1313.07
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m41.479s
user	15m1.461s
sys	3m34.303s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    12

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0164036177010E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8287841544342E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.3479645168849E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4275755120434E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3665393537540E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001831E-01 2.5981205001830E-01 2.2647841122453E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7260937288938E-13
           3 5.1328864163198E-02 5.1328864163200E-02 4.1772173440077E-14
           4 4.8060734194540E-02 4.8060734194540E-02 8.9514117762088E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.6533907062586E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    96.08
 Total threads   =                       12
 Avail threads   =                       12
 Mop/s total     =                 15092.36
 Mop/s/thread    =                  1257.70
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m37.177s
user	15m34.229s
sys	3m51.293s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    13

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0743904244589E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8519437906665E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4312643376559E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4206062002750E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.1751238318254E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7073463959190E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.7986992675280E-14
           4 4.8060734194540E-02 4.8060734194540E-02 7.2188804646845E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.0054846147235E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    95.88
 Total threads   =                       13
 Avail threads   =                       13
 Mop/s total     =                 15124.06
 Mop/s/thread    =                  1163.39
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m37.010s
user	16m34.062s
sys	4m26.463s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    14

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0550614888729E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8172043363181E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4331087504621E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4206062002750E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.5810757010015E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7006509198565E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.9068472893794E-14
           4 4.8060734194540E-02 4.8060734194540E-02 6.0638595903350E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.5926495101772E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    91.52
 Total threads   =                       14
 Avail threads   =                       14
 Mop/s total     =                 15844.77
 Mop/s/thread    =                  1131.77
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m32.599s
user	16m38.535s
sys	4m56.597s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    15

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0550614888729E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8635236087827E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2444192225482E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4294199248496E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.5107176111434E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.3674168224877E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7046682054940E-13
           3 5.1328864163198E-02 5.1328864163200E-02 4.1907358467391E-14
           4 4.8060734194540E-02 4.8060734194540E-02 6.7857476368035E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.4104259219330E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    85.65
 Total threads   =                       15
 Avail threads   =                       15
 Mop/s total     =                 16930.11
 Mop/s/thread    =                  1128.67
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m26.652s
user	16m55.750s
sys	4m43.157s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    16

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0550614888729E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8172043363181E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4294199248496E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4206062002750E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.5383439252987E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7006509198565E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.8798102839165E-14
           4 4.8060734194540E-02 4.8060734194540E-02 9.9620550412647E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.1674611376073E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    92.19
 Total threads   =                       16
 Avail threads   =                       16
 Mop/s total     =                 15730.25
 Mop/s/thread    =                   983.14
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m33.216s
user	19m2.863s
sys	5m47.461s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    17

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0550614888729E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8172043363181E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4294199248496E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.4025839181014E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.8588322430693E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7314501097437E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.7716622620652E-14
           4 4.8060734194540E-02 4.8060734194540E-02 8.3739013390341E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.5926495101772E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    89.27
 Total threads   =                       17
 Avail threads   =                       17
 Mop/s total     =                 16244.83
 Mop/s/thread    =                   955.58
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m30.291s
user	19m24.359s
sys	6m8.773s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    18

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 6.9970746821151E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8635236087827E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4220422736246E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3124725072330E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001831E-01 2.5981205001830E-01 1.9442957944748E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7180591576188E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.9609213003050E-14
           4 4.8060734194540E-02 4.8060734194540E-02 9.0957893855025E-15
           5 5.4833774913006E-01 5.4833774913010E-01 7.4104259219330E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    83.66
 Total threads   =                       18
 Avail threads   =                       18
 Mop/s total     =                 17334.26
 Mop/s/thread    =                   963.01
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m24.670s
user	19m41.413s
sys	5m41.415s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    19

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 6.9970746821151E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8635236087827E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.2789343206605E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4201978608183E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3124725072330E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.7733686916638E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7006509198565E-13
           3 5.1328864163198E-02 5.1328864163200E-02 4.0285138139621E-14
           4 4.8060734194539E-02 4.8060734194540E-02 1.2416474399257E-14
           5 5.4833774913006E-01 5.4833774913010E-01 7.3901788565725E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                    89.58
 Total threads   =                       19
 Avail threads   =                       19
 Mop/s total     =                 16188.38
 Mop/s/thread    =                   852.02
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m30.616s
user	21m44.111s
sys	6m55.817s
cannot open place file : turn into free-for-all mode


 NAS Parallel Benchmarks (NPB3.3-OMP-C) - SP Benchmark

 No input file inputsp.data. Using compiled defaults
 Size:  162x 162x 162
 Iterations:  400    dt:    0.0006700
 Number of available threads:    20

ROI begins
 Time step    1
 Time step   20
 Time step   40
 Time step   60
 Time step   80
 Time step  100
 Time step  120
 Time step  140
 Time step  160
 Time step  180
 Time step  200
 Time step  220
 Time step  240
 Time step  260
 Time step  280
 Time step  300
 Time step  320
 Time step  340
 Time step  360
 Time step  380
 Time step  400
ROI ends
 Verification being performed for class C
 accuracy setting for epsilon =  1.0000000000000E-08
 Comparison of RMS-norms of residual
           1 5.8816915818294E+02 5.8816915818290E+02 7.0550614888729E-14
           2 2.4544176035691E+02 2.4544176035690E+02 4.8287841544342E-14
           3 3.2938291918509E+02 3.2938291918510E+02 3.1926465753799E-14
           4 3.0819249718906E+02 3.0819249718910E+02 1.4146646223996E-13
           5 4.5972237991758E+02 4.5972237991760E+02 3.3124725072330E-14
 Comparison of RMS-norms of solution error
           1 2.5981205001830E-01 2.5981205001830E-01 1.8374663552179E-14
           2 2.5908889223146E-02 2.5908889223150E-02 1.7193982528313E-13
           3 5.1328864163198E-02 5.1328864163200E-02 3.9203657921108E-14
           4 4.8060734194539E-02 4.8060734194540E-02 1.2560852008551E-14
           5 5.4833774913006E-01 5.4833774913010E-01 7.3901788565725E-14
 Verification Successful


 SP Benchmark Completed.
 Class           =                        C
 Size            =            162x 162x 162
 Iterations      =                      400
 Time in seconds =                   100.78
 Total threads   =                       20
 Avail threads   =                       20
 Mop/s total     =                 14389.31
 Mop/s/thread    =                   719.47
 Operation type  =           floating point
 Verification    =               SUCCESSFUL
 Version         =                    3.3.1
 Compile date    =              25 Jul 2017

 Compile options:
    CC           = xlc
    CLINK        = $(CC)
    C_LIB        = -L/u/hluo/sys/lib -lm -lnuma
    C_INC        = -I../common
    CFLAGS       = -g -Wall -O3 -fopenmp -mcmodel=medium $(MyC...
    CLINKFLAGS   = -O3 -fopenmp -mcmodel=medium 
    RAND         = (none)

--------------------------------------
 Please send all errors/feedbacks to:
 Center for Manycore Programming
 cmp@aces.snu.ac.kr
 http://aces.snu.ac.kr
--------------------------------------


real	1m41.862s
user	23m46.579s
sys	10m7.587s
